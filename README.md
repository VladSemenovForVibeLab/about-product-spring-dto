## Описание

Данное приложение является примером Spring Boot приложения на языке Java версии 17. Оно использует следующие технологии:
- Spring Boot версии 3.1.4
- Springdoc версии 2.2.0
- Spring Data JPA
- Validation
- DevTools
- H2 Database
- MariaDB
- PostgreSQL
- Lombok
- Maven
- Docker
- Docker Compose

Приложение предоставляет API для управления продуктами. Возможны следующие операции:

- Создание продукта
- Получение продукта по идентификатору
- Удаление продукта по идентификатору
- Изменение продукта

## Инструкции по запуску

1. Установите [Java](https://www.java.com) версии 17 или выше
2. Установите [Docker](https://www.docker.com) и [Docker Compose](https://docs.docker.com/compose/install/)
3. Склонируйте репозиторий:

```bash
$ git clone git@gitlab.com:VladSemenovForVibeLab/about-product-spring-dto.git
```

4. Перейдите в директорию проекта:

```bash
$ cd crud-application
```

5. Соберите проект с помощью Maven:

```bash
$ mvn clean install
```

6. Запустите контейнеры с помощью Docker Compose:

```bash
$ docker-compose up 
```

7. API будет доступно по адресу [http://localhost:8080](http://localhost:8080)

## Документация API

Документация API доступна по адресу [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)