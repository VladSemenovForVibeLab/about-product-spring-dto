package com.semenov.crudapplication.util;

import com.semenov.crudapplication.dtos.ProductModelDto;
import com.semenov.crudapplication.models.product.ProductModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductDtoUtil implements EntityDtoUtil<ProductModelDto, ProductModel> {
    @Override
    public  ProductModelDto toDto(ProductModel entityModel) {
        ProductModelDto dto = new ProductModelDto();
        BeanUtils.copyProperties(entityModel,dto);
        return dto;

    }

    @Override
    public  ProductModel toEntity(ProductModelDto entityModelDto) {
        ProductModel product = new ProductModel();
        BeanUtils.copyProperties(entityModelDto,product);
        return product;
    }

    @Override
    public List<ProductModelDto> toDtoList(List<ProductModel> entityModelList) {
        List<ProductModelDto> productModelDtos = new ArrayList<>();
        for(ProductModel model : entityModelList){
            ProductModelDto dto = toDto(model);
            productModelDtos.add(dto);
        }
        return productModelDtos;
    }

    @Override
    public List<ProductModel> toEntityList(List<ProductModelDto> entityModelDtoList) {
        List<ProductModel> productModels = new ArrayList<>();
        for(ProductModelDto model : entityModelDtoList){
            ProductModel modelAdd = toEntity(model);
            productModels.add(modelAdd);
        }
        return productModels;
    }
}
