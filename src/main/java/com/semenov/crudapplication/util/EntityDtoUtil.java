package com.semenov.crudapplication.util;

import com.semenov.crudapplication.dtos.ProductModelDto;
import com.semenov.crudapplication.models.product.ProductModel;
import org.springframework.beans.BeanUtils;

import java.util.List;

public interface EntityDtoUtil<E,L> {
     E toDto(L entityModel);
    L toEntity(E entityModelDto);
    List<E> toDtoList(List<L> entityModelList);
    List<L> toEntityList(List<E> entityModelDtoList);
}
