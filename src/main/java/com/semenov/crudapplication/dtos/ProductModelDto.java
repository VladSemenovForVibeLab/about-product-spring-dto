package com.semenov.crudapplication.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProductModelDto {
    private Long id;
    private String name;
    private float price;
    private LocalDate fecha;
    private int antiquedad;
}
