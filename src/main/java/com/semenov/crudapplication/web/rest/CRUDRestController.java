package com.semenov.crudapplication.web.rest;

import com.semenov.crudapplication.service.interf.CRUDService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class CRUDRestController<E,K> {
    abstract CRUDService<E,K> getServiceForEntity();

    @PostMapping("/create")
    public ResponseEntity<E> createEntity(@RequestBody E entityForCreate){
        getServiceForEntity().createEntity(entityForCreate);
        return ResponseEntity.ok(entityForCreate);
    }
    @GetMapping("/find/{id}")
    public ResponseEntity<E> findEntityById(@PathVariable K id){
        E entityById = getServiceForEntity().findById(id);
        if(entityById==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(entityById);
    }
    @GetMapping("/all")
    public ResponseEntity<List<E>> findAll(){
        List<E> objects = getServiceForEntity().findAll();
        return ResponseEntity.ok(objects);
    }
    @PutMapping("/update")
    public ResponseEntity<E> updateEntity(@RequestBody E entityForUpdate){
        E updatedEntity = getServiceForEntity().updateEntity(entityForUpdate);
        return ResponseEntity.ok(updatedEntity);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteEntity(@PathVariable K id){
        E objectForDelete = getServiceForEntity().findById(id);
        getServiceForEntity().deleteEntity(objectForDelete);
        return ResponseEntity.notFound().build();
    }
}
