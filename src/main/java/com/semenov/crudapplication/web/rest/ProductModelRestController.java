package com.semenov.crudapplication.web.rest;

import com.semenov.crudapplication.dtos.ProductModelDto;
import com.semenov.crudapplication.service.interf.CRUDService;
import com.semenov.crudapplication.service.interf.ProductModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(ProductModelRestController.PRODUCT_REST_URL)
public class ProductModelRestController extends CRUDRestController<ProductModelDto, Long> {
    public static final String PRODUCT_REST_URL = "api/v1/product";
    private final ProductModelService productModelService;

    @Override
    CRUDService<ProductModelDto, Long> getServiceForEntity() {
        return productModelService;
    }
}
