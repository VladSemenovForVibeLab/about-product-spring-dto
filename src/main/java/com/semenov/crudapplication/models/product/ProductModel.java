package com.semenov.crudapplication.models.product;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "product_model")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(unique = true)
    private String name;
    private float price;
    private LocalDate fecha;
    private int antiquedad;

    public ProductModel(String name, float price, LocalDate fecha, int antiquedad) {
        this.name = name;
        this.price = price;
        this.fecha = fecha;
        this.antiquedad = antiquedad;
    }
}