package com.semenov.crudapplication.service.interf;

import com.semenov.crudapplication.dtos.ProductModelDto;
import com.semenov.crudapplication.models.product.ProductModel;

public interface ProductModelService extends CRUDService<ProductModelDto,Long> {
}
