package com.semenov.crudapplication.service.implementation;

import com.semenov.crudapplication.service.interf.CRUDService;
import com.semenov.crudapplication.util.EntityDtoUtil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCRUDService<E,K,L> implements CRUDService<E,K> {
    abstract JpaRepository<L,K> getRepositoryForEntity();
    abstract EntityDtoUtil<E,L> getEntityDtoUtil();
    @Override
    @Transactional
    public E createEntity(E entityDto){
        L entity = getEntityDtoUtil().toEntity(entityDto);
        L entitySave = getRepositoryForEntity().save(entity);
        return getEntityDtoUtil().toDto(entitySave);
    }

    @Override
    @Transactional(readOnly = true)
    public E findById(K id) {
        L entityFind = getRepositoryForEntity().findById(id).orElseThrow(null);
        return getEntityDtoUtil().toDto(entityFind);
    }

    @Override
    @Transactional(readOnly = true)
    public List<E> findAll() {
        List<L> objects = new ArrayList<>();
        getRepositoryForEntity().findAll().forEach(objects::add);
        List<E> entityDtoList = getEntityDtoUtil().toDtoList(objects);
        return entityDtoList;
    }

    @Override
    @Transactional
    public E updateEntity(E entityDto) {
        L entityUpdate = getEntityDtoUtil().toEntity(entityDto);
        L entitySave = getRepositoryForEntity().save(entityUpdate);
        E dto = getEntityDtoUtil().toDto(entitySave);
        return dto;
    }

    @Override
    @Transactional
    public void deleteEntity(E entityDto) {
        L entityForDelete = getEntityDtoUtil().toEntity(entityDto);
        getRepositoryForEntity().delete(entityForDelete);
    }
}
