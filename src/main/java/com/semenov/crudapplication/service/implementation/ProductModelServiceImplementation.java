package com.semenov.crudapplication.service.implementation;

import com.semenov.crudapplication.dtos.ProductModelDto;
import com.semenov.crudapplication.models.product.ProductModel;
import com.semenov.crudapplication.repositories.ProductModelRepository;
import com.semenov.crudapplication.service.interf.ProductModelService;
import com.semenov.crudapplication.util.EntityDtoUtil;
import com.semenov.crudapplication.util.ProductDtoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@RequiredArgsConstructor
public class ProductModelServiceImplementation extends AbstractCRUDService<ProductModelDto,Long,ProductModel> implements ProductModelService {
    private final ProductModelRepository productModelRepository;
    private final ProductDtoUtil productDtoUtil;
    @Override
    JpaRepository<ProductModel,Long> getRepositoryForEntity() {
        return productModelRepository;
    }

    @Override
    EntityDtoUtil<ProductModelDto,ProductModel> getEntityDtoUtil() {
        return productDtoUtil;
    }
}
